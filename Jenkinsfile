node {

  def mvnHome = tool 'Maven'

  stage("Checkout") {
    checkout scm
  }

  stage("Build") {
    try {
      sh "${mvnHome}/bin/mvn clean install"
      currentBuild.result = "success"
    } catch(exception) {
      currentBuild.result = "failure"
    }
  }

  stage("Comment on JIRA") {
    def jiraIssueNumber = getJiraIssueNumber()
    if (jiraIssueNumber) {
      def comment = "success".equalsIgnoreCase(currentBuild.result) ? "Successfully integrated" : "Integration failed";
      comment += " in [${JOB_BASE_NAME} ${BUILD_DISPLAY_NAME}|${BUILD_URL}]:" 
      comment += getChangesAsComment().replaceAll("BASE_COMMIT_URL", getBaseCommitUrl())
      jiraAddComment site: "LOCAL", idOrKey: jiraIssueNumber, comment: comment
    }
  }

}

def getChangesAsComment() {
  def changes = "\n"
  for (changeSet in currentBuild.changeSets) {
    for (item in changeSet.items) {
      changes += "- *Commit ${item.commitId} by ${item.author}*\n${item.msg} ([Details|BASE_COMMIT_URL${item.commitId}])\n"
    }
  }
  return changes
}

def getBaseCommitUrl() {
  sh "git config --get remote.origin.url > .git/remote-url"
  def gitRemoteUrl = readFile('.git/remote-url').trim()
  return gitRemoteUrl.replaceAll("\\.git", "/commits/")
}

@NonCPS
def getJiraIssueNumber() {
  def matcher = (BRANCH_NAME =~ "^([A-Za-z]+-\\d)")
  return matcher ? matcher[0][1] : null
}